## Dataeaze Coding Test ##


# I have use Jupyter Notebook for coding test
Step 1: Initiated Spark and imported some libraries of spark.
Step 2: Added two file consumerInternetFile and startUpFile to local system.
Step 3: Perform read operation on both the files
Step 4: Combine the consumerInternetFile and startUpFile.


# Executed Queries : 

# Answers for below questions and there output :

Q1. How many startups are there in Pune City?
Ans -  105
Query - df.registerTempTable("df")
        q1 = spark.sql("select count(*) AS Satrtups from df where city = 'Pune' ;")
        q1.show()


Q2. How many startups in Pune got their Seed/ Angel Funding?
Ans - 5
Query - df.filter( (df.InvestmentnType == 'Seed') | (df.InvestmentnType == 'Angel Funding') ).count()


Q3. What is the total amount raised by startups in Pune City? Hint - use regex_replace to get rid of null
Ans - 633082000.0
Query - import pyspark.sql.functions as F     
        punestartups2.agg(F.sum("Amount_in_USD")).collect()[0][0]


Q4. What are the top 5 Industry_Vertical which has the highest number of startups in India?
Ans - 
+-----------------+-----+
|Industry_Vertical|count|
+-----------------+-----+
|Consumer Internet|941  |
|Technology       |478  |
|eCommerce        |186  |
|nan              |171  |
|Healthcare       |70   |
+-----------------+-----+
only showing top 5 rows

Query - industry_verticle.groupBy('Industry_Vertical').count().orderBy(col('count').desc()).show(5,truncate=False)


Q5. Find the top Investor(by amount) of each year.
Ans - 
+----+------------------+-----+----+------------+-----------------+--------------------+---------+--------------------+---------------+-------------+-------+
|Date|max(Amount_in_USD)|Sr_No|Date|Startup_Name|Industry_Vertical|         SubVertical|     City|      Investors_Name|InvestmentnType|Amount_in_USD|Remarks|
+----+------------------+-----+----+------------+-----------------+--------------------+---------+--------------------+---------------+-------------+-------+
|2016|         200000000| 1376|2016|    GoPigeon|Consumer Internet|on-demand logisti...|Bangalore|Nexus Venture Par...| Private Equity|      1500000|    nan|
|2016|         200000000| 2012|2016|   Instaproc|        ECommerce|B2B Procurement M...|    Noida|      Dinesh Agarwal|   Seed Funding|            0|    nan|
+----+------------------+-----+----+------------+-----------------+--------------------+---------+--------------------+---------------+-------------+-------+
only showing top 2 rows

Query - top_investor_eachyear =dfmaxamountofyear.join(dfonlyyear, dfmaxamountofyear.Date == dfonlyyear.Date).show(2)


package com.sunbeam.edbda;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class DataeazeReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
	private IntWritable sumWr = new IntWritable();
	@Override
	protected void reduce(Text keyWr, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
		int sum = 0;
		for (IntWritable countWr : values)
			sum = sum + countWr.get();
		sumWr.set(sum);
		context.write(keyWr, sumWr);
	}
}

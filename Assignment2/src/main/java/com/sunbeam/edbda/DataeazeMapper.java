package com.sunbeam.edbda;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class DataeazeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
	private IntWritable oneWr = new IntWritable(1);
	private Text wordWr = new Text();

	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		String line = value.toString();
		String[] words = line.toLowerCase().split("[^a-z]");
		for (String word: words) {
			wordWr.set(word);
			context.write(wordWr, oneWr);
		}
	}
}

package com.sunbeam.edbda;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MRDriver extends Configured implements Tool {
	public static void main(String[] args) {
		try {
			// create Configuration object from generic options
			GenericOptionsParser parser = new GenericOptionsParser(args);
			Configuration conf = parser.getConfiguration();
			// execute Tool.run()
			int ret = ToolRunner.run(conf, new MRDriver(), args);
			System.exit(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// create MR job and submit to cluster
	@Override
	public int run(String[] args) throws Exception {
		// check if valid command line args
		if(args.length != 2) {
			System.err.println("Invalid command line arguments.");
			System.exit(1);
		}
		// get the configuration
		Configuration conf = this.getConf();
		// create MR job
		Job job = Job.getInstance(conf, "dataeaze");
		// set the jar in which mapper and reducer classes are available
		job.setJarByClass(MRDriver.class);
		// set mapper class and its output
		job.setMapperClass(WordCountMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		// set reducer class and its output
		job.setReducerClass(WordCountReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		// set input & output format
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		// set input & output path
		FileInputFormat.addInputPaths(job, args[0]); // hdfs path of dir or file -- path must exists
		FileOutputFormat.setOutputPath(job, new Path(args[1])); // hdfs path of output dir -- dir will be created at runtime 
		// set combiner & partitioner (if required)
		// submit the job and wait for completion
		job.submit();
		boolean success = job.waitForCompletion(true);
		int ret = success ? 0 : 1;
		return ret;
	}
}


